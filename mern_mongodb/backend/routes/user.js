const router = require("express").Router();

const { User } = require("../models/user");

router.get("/", (req, res) => {
  User.find()
    .sort({ _id: -1 })
    .then((data) => {
      return res.json(data);
    })
    .catch((e) => {
      return res.status(400).json(e);
    });
});

router.post("/post", (req, res) => {
  const user = new User({
    title: req.body.title,
    author: req.body.author,
    price: req.body.price,
  });
  user
    .save()
    .then((data) => res.json(data))
    .catch(() => res.status(400).json("ERROR Spotted"));
});

//get one data
router.get("/:id", (req, res) => {
  User.findById(req.params.id)
    .then((data) => {
      return res.json(data);
    })
    .catch((e) => res.status(400).json(e));
});

//deleteById
router.delete("/:id", (req, res) => {
  User.findByIdAndDelete(req.params.id)
    .then((data) => {
      return res.json({
        message: "removed",
        deleted: data,
      });
    })
    .catch((e) => res.status(400).json(e));
});

//updateById
router.put("/:id", (req, res) => {
  User.update(
    { _id: req.params.id },
    {
      $set: {
        title: req.body.title,
        author: req.body.author,
        price: req.body.price,
      },
    }
  )
    .then((data) => {
      console.log("updated !");
      return res.json(data);
    })
    .catch((e) => res.status(400).json(e));
});

module.exports = router;
