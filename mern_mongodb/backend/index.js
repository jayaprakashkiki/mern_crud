require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors')
const app = express();
const PORT = process.env.PORT || 8000;
const userRouter = require("./routes/user");
app.use(cors())
app.use(express.json());

app.listen(PORT, () => {
  console.log("Server runnning on localhost", 8000);
});

mongoose
  .connect("mongodb://localhost:27017/test", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("DB CONNECTED...");
  })
  .catch((e) => {
    console.log("ERROR ON DB CONNECT..");
  });

app.use("/api", userRouter);
