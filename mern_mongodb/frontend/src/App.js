import React, { useEffect, useState } from "react";
import { Button, Col, Container, Form, ListGroup, Row } from "react-bootstrap";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  const dbString = "http://localhost:8000/api";

  //state
  const [post, setPost] = useState({
    title: "",
    author: "",
    price: "",
  });

  const [getData, setGetData] = useState([]);
  const [button, setButton] = useState({
    show: false,
    id: "",
  });

  const fetchData = () => {
    axios
      .get(dbString)
      .then((res) => {
        console.log(res.data);
        setGetData(res.data);
      })
      .catch((e) => console.log(e));
  };

  useEffect(() => {
    fetchData();
    return () => {
      fetchData();
    };
  }, []);

  const PostData = () => {
    axios
      .post(`${dbString}/post`, post)
      .then((res) => {
        console.log(res);
        toast.success("Book on self !", {
          autoClose: 2000,
        });
        setPost({
          title: "",
          author: "",
          price: "",
        });
        fetchData();
      })
      .catch((e) => {
        console.log(e)
        toast(e);
      });
  };

  const Delete = (id) => {
    axios
      .delete(`${dbString}/${id}`)
      .then((res) => {
        console.log(res);
        toast.error("Deleted Successfully !", {
          autoClose: 2000,
        });
        fetchData();
      })
      .catch((e) => console.log(e));
  };

  const Update = (item) => {
    console.log(item._id);
    setPost({
      title: item.title,
      author: item.author,
      price: item.price,
    });
    setButton({ ...button, show: true, id: item._id });
  };

  const upDateData = () => {
    const id = button.id;
    console.log(id);
    axios
      .put(`${dbString}/${id}`, {
        title: post.title,
        author: post.author,
        price: post.price,
      })
      .then((res) => {
        setPost({
          title: "",
          author: "",
          price: "",
        });
        setButton({ ...button, show: false });
        toast.success("Updated !", {
          autoClose: 2000,
        });
        fetchData();
      })
      .catch((e) => console.log(e));
  };

  return (
    <div>
      <ToastContainer />
      <section style={{ backgroundColor: "#16161E", paddingBottom: 200 }}>
        <Container>
          <div>
            <p className="h2 pt-5 text-warning text-center">BooK Store</p>

            <p className="h6 pt-2 text-white text-center">
              Crud Operation using - React | Express | Mongo | Node
            </p>
          </div>
          <Row>
            <Col className="pt-5">
              <h5 className="text-white">Add Books</h5>
              <Form className="pt-3">
                <Form.Group>
                  <Form.Control
                    onChange={(e) =>
                      setPost({ ...post, title: e.target.value })
                    }
                    placeholder="Title "
                    value={post.title}
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Control
                    onChange={(e) =>
                      setPost({ ...post, author: e.target.value })
                    }
                    placeholder="Author "
                    value={post.author}
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Control
                    onChange={(e) =>
                      setPost({ ...post, price: e.target.value })
                    }
                    placeholder="price "
                    value={post.price}
                  />
                </Form.Group>

                {!button.show ? (
                  <Button onClick={() => PostData()} variant="success">
                    Submit
                  </Button>
                ) : (
                  <Button onClick={() => upDateData()} variant="success">
                    update
                  </Button>
                )}
              </Form>
            </Col>
            <Col className="pt-5">
              <h5 className="text-white">List Of Books</h5>

              {getData.length ? (
                getData.map((item, index) => {
                  return (
                    <ListGroup className="pt-3" key={index}>
                      <div className="rounded">
                        <ListGroup.Item className="row d-flex justify-content-between align-items-center">
                          <div>
                            <p>Title : {item.title}</p>
                            <p>
                              Author : {item.author} &nbsp; &nbsp; &nbsp; Price
                              : {item.price}
                            </p>
                          </div>
                          <div>
                            <Button
                              onClick={() => Delete(item._id)}
                              className="bg-danger border-0"
                            >
                              Delete
                            </Button>
                            <Button
                              onClick={() => Update(item)}
                              className="bg-warning ml-4 border-0"
                            >
                              Update
                            </Button>
                          </div>
                        </ListGroup.Item>
                      </div>
                    </ListGroup>
                  );
                })
              ) : (
                <h5 className="text-danger">No document present</h5>
              )}
            </Col>
          </Row>
        </Container>
      </section>
    </div>
  );
}

export default App;
